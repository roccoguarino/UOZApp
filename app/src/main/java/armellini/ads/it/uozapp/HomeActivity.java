package armellini.ads.it.uozapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Created by giuseppevecchi on 12/01/17.
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "HomeActivity";
    private boolean backPressed = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //itero sugli id dei bottoni che mi interessa gestire e imposto come ascoltatore della pressione l'activity stessa
        int buttons[] = new int[]{R.id.image_button_contatti,R.id.image_button_cronologia,R.id.image_button_impostazioni};
        for (int i =0; i< buttons.length; i++){
            ImageButton imageButton = (ImageButton) findViewById(buttons[i]);
            imageButton.setOnClickListener(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        backPressed = false;
    }

    @Override
    public void onClick(View v) {
        int buttonId = v.getId();
        switch (buttonId){
            case R.id.image_button_contatti:
                imageButtonContattiClicked();
                break;
            case R.id.image_button_cronologia:
                imageButtonCronologiaClicked();
                break;
            case R.id.image_button_impostazioni:
                imageButtonImpostazioniClicked();
                break;
        }
    }

    private void imageButtonContattiClicked(){
        Log.i(TAG, "gestisto la pressione sull'icona contatti");
        Utils.openActivity(this,ContattiActivity.class);
    }

    private void imageButtonCronologiaClicked(){
        Log.i(TAG, "gestisto la pressione sull'icona cronologia");
        Toast.makeText(this,"Disponibile a breve", Toast.LENGTH_SHORT).show();
    }

    private void imageButtonImpostazioniClicked(){
        Log.i(TAG, "gestisto la pressione sulll'icona impostazioni");
        Utils.openActivity(this,SettingsActivity.class);
    }

    @Override
    public void onBackPressed() {
        if(backPressed){
            super.onBackPressed();
        }else {
            Toast.makeText(this,"Premi nuovamente per uscire", Toast.LENGTH_SHORT).show();
            backPressed = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId){
            case R.id.action_impostazioni:
                Utils.openActivity(this,SettingsActivity.class);
                break;
        }
        return true;
    }
}
