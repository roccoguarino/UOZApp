package armellini.ads.it.uozapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class ContattiActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    ListView listaContatti;
    //MqttAndroidClient mqttAndroidClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_contatti);
        listaContatti = (ListView)findViewById(R.id.listViewContatti);

        ContattiAdapter adapter = new ContattiAdapter(getApplicationContext());
        listaContatti.setAdapter(adapter);
        listaContatti.setOnItemClickListener(this);

        ImageButton nuovoMessaggio = (ImageButton)findViewById(R.id.button_nuovo_messaggio);
        nuovoMessaggio.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i("ROCCO", "hai selezionato il numero " + position);
        //Toast.makeText(ContattiActivity.this, "hai selezionato il numero " + position , Toast.LENGTH_SHORT).show();

        ListAdapter contattiAdapter = listaContatti.getAdapter();
        JSONObject elemento = (JSONObject) contattiAdapter.getItem(position);
        String nome = null;
        String cognome = null;
        try {
            nome = elemento.getString("nome");
            cognome = elemento.getString("cognome");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("ROCCO", "hai selezionato:  " + nome +  " " + cognome);
        //Toast.makeText(ContattiActivity.this, "hai selezionato:  " + nome +  " " + cognome , Toast.LENGTH_LONG).show();


        //Lancio un anctivity (Finestra) di tipo Chat
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(),ChatActivity.class);
        intent.putExtra("nome",nome);
        intent.putExtra("cognome",cognome);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case  R.id.button_nuovo_messaggio:
                Toast.makeText(ContattiActivity.this, "Mi hai premuto stronzo", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId){
            case R.id.action_save:
                menuImpostazioniPremuto();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private  void menuImpostazioniPremuto(){

    }

}
