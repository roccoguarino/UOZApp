package armellini.ads.it.uozapp;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by giuseppevecchi on 10/01/17.
 */
public class ContattiAdapter implements ListAdapter {

//  String[] contatti = new String[]{"Rocco", "Simone"};
    JSONArray contatti;
    Context context;
    LayoutInflater layoutInflater;

    public ContattiAdapter(Context context){

        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.contatti = new JSONArray();

        try {
            loadContattiFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    protected void loadContattiFromFile() throws IOException {
        InputStream inputStream = context.getResources().openRawResource(R.raw.contatti_4g);
        if(inputStream == null){
            Log.i("","file json dei contatti inesistente");
            return;
        }

        ByteArrayOutputStream json = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            json.write(buffer, 0, length);
        }

        try {
            contatti = new JSONArray(json.toString("UTF-8"));
        } catch (JSONException e) {
            Log.i("","formato json errato");
            e.printStackTrace();
        }

    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }
    @Override
    public int getCount() {
        return contatti.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return contatti.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RelativeLayout item =(RelativeLayout) layoutInflater.inflate(R.layout.item_contatto,null,false);
        TextView nome = (TextView) item.findViewById(R.id.textviewNome);
        TextView cognome = (TextView) item.findViewById(R.id.textviewCognome);
        try {
            JSONObject obj = contatti.getJSONObject(position);
            nome.setText(obj.getString("nome"));
            cognome.setText(obj.getString("cognome"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return item;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
