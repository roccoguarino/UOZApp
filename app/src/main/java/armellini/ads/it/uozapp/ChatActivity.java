package armellini.ads.it.uozapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by giuseppevecchi on 10/01/17.
 */
public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //recupero i valori di nome e cognome inviati con l'intent per aprire l'activity
        String nome = getIntent().getStringExtra("nome");
        String cognome = getIntent().getStringExtra("cognome");

        getSupportActionBar().setTitle(nome + " " + cognome);
    }
}
